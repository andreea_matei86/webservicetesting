public class Person {

    private int id;
    private String firstName;
    private String lastName;

    public Person() {}

    public Person (Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
    }

    public static class Builder {
        private int id;
        private String firstName;
        private String lastName;

        public Builder withId(int id) {
            this.id= id;
            return this;
        }

        public Builder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder withLastName (String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Person build () {
            return new Person(this);
        }
    }
}
