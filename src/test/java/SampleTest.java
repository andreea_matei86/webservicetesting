import com.google.gson.Gson;
import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;

public class SampleTest {


    @Before
    public void setup() {
        RestAssured.baseURI = "http://localhost:8080/employees";
    }
    @Test
    public void statusTest() {
        /*Given I have the URL endpoint for the employees resource
        When I make a get request
        Then I receive a 200 status code*/

        given().
                when().get()
                    .then().assertThat().statusCode(200);
    }

    @Test
    public void postTest() {

        /*Given I access the employee REST Endpoint
        And I make a new post for a new person
        Then I receive a 201 created status code
        And the response contains the id, firstName and lastName of the person*/

        int id = new Random().nextInt(500) +1 ;
        String firstName ="Ryan".concat(String.valueOf(id));
        String lastName = "Gossling".concat(String.valueOf(id));

        Person person = new Person.Builder()
                .withId(id)
                .withFirstName(firstName)
                .withLastName(lastName)
                .build();

        given().contentType("application/json").body(person)
                    .when().post()
                        .then().assertThat().statusCode(201)
                        .and().body("id", equalTo(id))
                        .body("firstName", equalTo(firstName))
                        .body("lastName", equalTo(lastName));
    }

    @Test
    public void negativeCase() {
        /*Given I access the employee REST Endpoint
        And I make a new post request with an invalid body
        Then I receive a 400 Bad Request status*/

        given().contentType("application/json").body("")
                .when().post()
                    .then().assertThat().statusCode(400);
    }

    @Test
    public void putTest() {
          /*Given I access the employee REST Endpoint
        And I make a new put request for an ID with a new firstName
        Then the JSON representation of the person was updated correctly */

        int id = 2;
        String newFirstName = "TESSST";
        Person person = new Person.Builder().withId(id).withFirstName(newFirstName).build();

        given().contentType("application/json")
                .when().body(person).put(String.valueOf(id))
                    .then().assertThat().body("firstName", equalTo(newFirstName));
    }

    @Test
    public void sizeTest() {
        when()
                .get().
                    then().assertThat().body("",hasSize(4));
    }

    @Test
    public void singleFieldVerification() {
        String expectedValue = "John";
        String actualValue = given().get("1").then().extract().path("firstName");

        Assert.assertEquals(expectedValue, actualValue);
    }

    @Test
    public void testWithStringId() {
        HashMap json = new HashMap();
        json.put("id", "12875876");
        json.put("firstName", "School");
        json.put("lastName", "Of automation");

        String json1 = "{\n" +
                "        \"id\": \"1285\",\n" +
                "        \"firstName\": \"School\",\n" +
                "        \"lastName\": \"Of automation\"\n" +
                "    }";
        given().contentType("application/json")
                .when().body(json).post()
                    .then().assertThat().statusCode(400);
    }

    @Test
    public void testWithoutField() {
        Person person = new Person.Builder().build();

        given().header("Content-Type", "application/json").when().body(person).post()
                .then().assertThat().statusCode(400);
    }

}
